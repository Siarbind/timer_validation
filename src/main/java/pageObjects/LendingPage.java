package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LendingPage {
	public WebDriver driver;

	By Value = By.cssSelector("#start_a_timer");
	By GoButton = By.cssSelector("#timergo");

	public LendingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub

		this.driver = driver;

	}
	
	

	public WebElement getValue() {
		return driver.findElement(Value);
	}

	public WebElement getButton() {
		return driver.findElement(GoButton);
	}
}
