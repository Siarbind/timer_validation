package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Loadpage {
	public WebDriver driver;
	
	By progressText=By.id("progressText");
	
	
	public Loadpage(WebDriver driver) {
		this.driver=driver;
	}

	public WebElement getPageText() {
		return driver.findElement(progressText);
		
	}
}
