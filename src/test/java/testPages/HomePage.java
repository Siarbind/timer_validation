package testPages;

import static org.testng.Assert.assertEquals;


import resources.base;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.LendingPage;
import pageObjects.Loadpage;

public class HomePage extends base {

	public WebDriver driver;
	 public static Logger log = LogManager.getLogger(base.class.getName());

	@BeforeMethod
	public void initialize() throws IOException {

		driver = initializeDriver();
         log.info("Driver is initialized");
		driver.get(prop.getProperty("url"));
		log.info("Navigated to e.ggtimer page");
	}

	@Test
	public void basePageNavigation() throws InterruptedException {
		//driver.get(prop.getProperty("url"));
		LendingPage l = new LendingPage(driver);
		l.getValue().clear();
		l.getValue().sendKeys(prop.getProperty("value"));
		l.getButton().click();
		Loadpage lp=new Loadpage(driver);
		WebElement element = lp.getPageText();		
		String temp = element.getText();
		Set<String> set = new LinkedHashSet<String>();
		Set<String> set1 = new LinkedHashSet<String>();
		while (!temp.equals("1 second")) {
			set.add(temp);
			set1.add(element.getText());
			temp = element.getText();
		}
		if (temp.equals("1 second")) {
			set.add(temp);
			set1.add(element.getText());
		}

		if (isAlertPresents()) {
			driver.switchTo().alert().accept();
			
		}

		set.add(element.getText());
		set1.add(element.getText());
		log.info(set);
		log.info(set1);
		
		assertEquals(set, set1);
		
	}
	
/* 	@Test
	public void basePageNavigationFailed() throws InterruptedException {
		//This test case for failed scenarios to check how the report is getting generated.Entered incorrect URL
		driver.get(prop.getProperty("url1"));
	} */
	@AfterMethod
	public void teardown() {
		driver.close();
	}
}
